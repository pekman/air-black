# Air-Black

Port do tema KDE Air Black para o gerenciador de janelas Pekwm.

## Instalando
Baixe o arquivo, descompacte e mova para `~/.pekwm/themes`

## Imagens

![menu](https://notabug.org/pekwm-themes/Air-Black/raw/master/scrots/168196-1.png)

![Janela sem foco](https://notabug.org/pekwm-themes/Air-Black/raw/master/scrots/168196-2.png)

![Janela com foco](https://notabug.org/pekwm-themes/Air-Black/raw/master/scrots/168196-3.png)